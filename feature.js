// shortcut functions
function qs(s) { return document.querySelector(s); }
function qsa(s) { return document.querySelectorAll(s); }
function f(l) { q(l).focus(); }

function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

ready(function(){

  qs("#featured").innerHTML = 
    '<a class="navarrow" id="prev_stanza" href="#">&#x21e7;</a>' +
    qs("#s1_1").innerHTML +
    '<a class="navarrow" id="next_stanza" href="#s1_2">&#x21e9;</a>'
    ;
  
  qs("body").classList.add("featuremode");

});
