#!/usr/bin/env perl

use warnings;
use JSON;

undef $/;

open (my $ed1, "<:encoding(UTF-8)", "rubaiyat_1ed.json") or die "nope";
open (my $ed5, "<:encoding(UTF-8)", "rubaiyat_5ed.json") or die "nay";

my @q1 = @{ decode_json <$ed1> };
my @q5 = @{ decode_json <$ed5> };

close ($ed1);
close ($ed5);

print "<h2>First Edition</h2>\n\n";
my $n = 0;
foreach my $quatrain (@q1) {
  $n++;

  s/--/&mdash;/g for @{$quatrain};

  print "<p id=\"1_$n\" class=\"stanza\">\n";
  print "<span class=\"number\">" . romanize($n) . ".</span><br>\n";
  print "<span>" . @{$quatrain}[0] . "</span><br>\n";
  print "<span>" . @{$quatrain}[1] . "</span><br>\n";
  print "<span class=\"off\">" . @{$quatrain}[2] . "</span><br>\n";
  print "<span>" . @{$quatrain}[3] . "</span>\n";
  print "</p>\n\n";
}

print "<h2>Fifth Edition</h2>\n\n";
$n = 0;
foreach my $quatrain (@q5) {
  $n++;

  s/--/&mdash;/g for @{$quatrain};

  print "<p id=\"5_$n\" class=\"stanza\">\n";
  print "<span class=\"number\">" . romanize($n) . ".</span><br>\n";
  print "<span>" . @{$quatrain}[0] . "</span><br>\n";
  print "<span>" . @{$quatrain}[1] . "</span><br>\n";
  print "<span class=\"off\">" . @{$quatrain}[2] . "</span><br>\n";
  print "<span>" . @{$quatrain}[3] . "</span>\n";
  print "</p>\n\n";
}

#print "\n";



sub romanize {
  
  use integer;
  
  my $num = shift;
  my $remainder = $num;
  my $roman_numeral = "";
  
  %rcode = (
    1000 => "M",
    500 => "D",
    100 => "C",
    50 => "L",
    10 => "X",
    5 => "V",
    1 => "I"
  );
  
  foreach my $component (sort {$b <=> $a} keys %rcode) {
    $roman_numeral .= $rcode{$component} x ($remainder / $component);
    $remainder = $remainder % $component;
    
    foreach my $mult (100, 10, 1) {
      if ( ( $component == (10 * $mult) || $component == (5 * $mult) ) && ($component - $mult <= $remainder) ) {
        $roman_numeral .= $rcode{$mult} . $rcode{$component};
        $remainder -= $component - $mult;
        last;  # because if this condition is true for any of the multipliers then we know it can't be for any of the other multipliers -- this only saves a little time, but we might as well
      }
    }
    
  }
  
  return $roman_numeral;
}
