#!/usr/bin/perl

print "[\n  [\n";

while (<>) {
  
  if (/^$/) {

    print "  ],\n  [\n";

  } else {

    chomp;
    
    s/^\s*//;
    s/\s*$//;
    
    print "    \"$_\",\n";

  }

}

print "  ]\n]\n";
